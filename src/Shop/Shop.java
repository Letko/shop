package Shop;
import ShopBackEnd.*;
import java.util.*;

public class Shop {

    public static void main(String[] args) {

        //Lists
        List<Category> categories = new ArrayList<>();
        List<Product> products = new ArrayList<>();
        Queue<List<Product>> orderList = new LinkedList<>();
        List<List<Product>> approvedList = new ArrayList<>();
        List<User> users = Arrays.asList(new User(new ArrayList<>()), new User(new ArrayList<>()), new User(new ArrayList<>())); //3 different users in shop (can be more)

        //Scanners
        Scanner scannerString = new Scanner(System.in);
        Scanner scannerInt = new Scanner(System.in);
        Scanner scannerDouble = new Scanner(System.in);

        //Administrator adds products and categories
        createCategoriesFromConsole(categories, scannerString);
        addProductFromConsole(categories, products, scannerString, scannerInt, scannerDouble);
        upgradeOrDeleteCategoryFromConsole(categories, scannerString, scannerInt);
        upgradeOrDeleteProductFromConsole(categories, products, scannerString, scannerInt, scannerDouble);


        //User makes an order
        for(User user : users) {
            searchProduct(products, scannerString, user);
            sortProductsFromConsole(user, products, scannerString);
            user.readSortedProducts(products);
            addProductToBasketFromConsole(user, products, user.getBasket(), scannerString, scannerInt);
            user.addOrderToOrderList(orderList, user.getBasket());
        }

        //Administrator manages orders
        Administrator.viewPlacedOrders(orderList);
        approveOrders(orderList, approvedList, scannerString);
        Administrator.viewApprovedOrders(approvedList);
        Administrator.viewProcessingOrders(orderList);
        Administrator.viewTotalCost(approvedList, orderList);

    }

    //Console interface

    private static void searchProduct(List<Product> products, Scanner scannerString, User user) {
        System.out.println("[User] Search product? If yes type \"y\". If not type another symbol or press Enter");
        String searchProduct = scannerString.nextLine();
        if(searchProduct.equalsIgnoreCase("y")) {
            System.out.println("Search by name or category? (name/category)");
            String search = scannerString.nextLine();
            switch (search) {
                case "name" :
                    searchProductByNameFromConsole(user, products, scannerString);
                    break;
                case "category" :
                    searchProductByCategoryFromConsole(user, products, scannerString);
                default :
                    System.out.println("Can't find product by that parameter");
            }


        }
    }


    private static void upgradeOrDeleteCategoryFromConsole(List<Category> categories, Scanner scannerString, Scanner scannerInt) {
        String str = "";
        while(!str.equalsIgnoreCase("y")) {
            System.out.println("[Administrator] Upgrade or delete category? (upgrade/delete/exit)");
            String upgradeOrDelete = scannerString.nextLine();
            switch (upgradeOrDelete) {
                case "upgrade":
                    System.out.println("Category index from category list");
                    int index = scannerInt.nextInt();
                    System.out.println("Set category name");
                    String name = scannerString.nextLine();
                    Administrator.upgradeCategory(categories, index, name);
                    break;
                case "delete":
                    System.out.println("Category index from category list");
                    index = scannerInt.nextInt();
                    Administrator.deleteCategory(categories, index);
                    break;
                case "exit":
                    System.out.println("Category list has not changed");
                    break;
                default:
                    System.out.println("Wrong command");
            }
            System.out.println("FINISH? If yes type \"y\". If not type another symbol or press Enter");
            str = scannerString.nextLine();
        }
        Administrator.readAllCategories(categories);
    }

    private static void upgradeOrDeleteProductFromConsole(List<Category> categories, List<Product> products, Scanner scannerString, Scanner scannerInt, Scanner scannerDouble) {
        String str = "";
        while(!str.equalsIgnoreCase("y")) {
            System.out.println("[Administrator] Upgrade or delete product? (upgrade/delete/exit)");
            String upgradeOrDelete = scannerString.nextLine();
            switch (upgradeOrDelete) {
                case "upgrade":
                    System.out.println("Product index from product list");
                    int productIndex = scannerInt.nextInt();
                    System.out.println("Category index from category list");
                    int categoryIndex = scannerInt.nextInt();
                    System.out.println("Set product name");
                    String name = scannerString.nextLine();
                    System.out.println("Set product price");
                    double price = scannerDouble.nextDouble();
                    System.out.println("Set product quantity");
                    int quantity = scannerInt.nextInt();
                    Administrator.upgradeProduct(products, productIndex, categories, categoryIndex, name, price, quantity);
                    break;
                case "delete":
                    System.out.println("Product index from product list");
                    int index = scannerInt.nextInt();
                    Administrator.deleteProduct(products, index);
                    break;
                case "exit":
                    System.out.println("Product list has not changed");
                    break;
                default:
                    System.out.println("Wrong command");
            }
            System.out.println("FINISH?  If yes type \"y\". If not type another symbol or press Enter");
            str = scannerString.nextLine();
        }
        Administrator.readAllProducts(products);
    }

    private static void approveOrders(Queue<List<Product>> orderList, List<List<Product>> approvedList, Scanner scannerString) {
        System.out.println("[Administrator] Approve order");
        String str = "";
        while(!str.equalsIgnoreCase("n") && orderList.size() > 0) {
            System.out.println("Approve next order?  If yes type \"y\". If not type \"n\"");
            str = scannerString.nextLine();
            if(str.equalsIgnoreCase("y")) {
                Administrator.approveOrders(approvedList, orderList);
            }
        }
    }

    private static void addProductToBasketFromConsole(User user, List<Product> products, List<Product> basket, Scanner scannerString, Scanner scannerInt) {
        System.out.println("[User] Add products to basket");
        String str = "";
        while(!str.equalsIgnoreCase("y")) {
            System.out.println("Product index");
            int productIndex = scannerInt.nextInt();
            user.addProductToBasket(basket, products, productIndex);
            System.out.println("FINISH?  If yes type \"y\". If not type another symbol or press Enter");
            str = scannerString.nextLine();
        }
    }

    private static void sortProductsFromConsole(User user, List<Product> products, Scanner scannerString) {
        System.out.println("[User] Sort products?  If yes type \"y\". If not type another symbol or press Enter");
        String sortQuestion = scannerString.nextLine();
        if(sortQuestion.equalsIgnoreCase("y")) {
            System.out.println("Sort by name or price ? (name/price)");
            String sortAnswer = scannerString.nextLine();
            if(sortAnswer.equalsIgnoreCase("name")) {
                user.sortProductByName(products);
            } else if(sortAnswer.equalsIgnoreCase("price")) {
                user.sortProductByPrice(products);
            }
        }
    }

    private static void searchProductByCategoryFromConsole(User user, List<Product> products, Scanner scannerString) {
        System.out.println("[User] Search product by category name");
        String str = "";
        while(!str.equalsIgnoreCase("y")) {
            System.out.println("Category name");
            String categoryName = scannerString.nextLine();
            user.searchProductByCategory(products, categoryName);
            System.out.println("FINISH?  If yes type \"y\". If not type another symbol or press Enter");
            str = scannerString.nextLine();
        }
    }

    private static void searchProductByNameFromConsole(User user, List<Product> products, Scanner scannerString) {
        System.out.println("[User] Search product by name");
        String str = "";
        while(!str.equalsIgnoreCase("y")) {
            System.out.println("Product name");
            String productName = scannerString.nextLine();
            user.searchProductByName(products, productName);
            System.out.println("FINISH?  If yes type \"y\". If not type another symbol or press Enter");
            str = scannerString.nextLine();
        }
    }

    private static void addProductFromConsole(List<Category> categories, List<Product> products, Scanner scannerString, Scanner scannerInt, Scanner scannerDouble) {
        System.out.println("[Administrator] Create and add new product to product list. Set product name, category index from category list, price and quantity");
        String str = "";
        while(!str.equalsIgnoreCase("y")) {
            System.out.println("Product name");
            String productName = scannerString.nextLine();
            System.out.println("Category index from category list");
            int index = scannerInt.nextInt();
            System.out.println("Product price");
            double price = scannerDouble.nextDouble();
            System.out.println("Product quantity");
            int quantity = scannerInt.nextInt();
            Administrator.addProduct(products, categories, productName, index, price, quantity);
            System.out.println("FINISH?  If yes type \"y\". If not type another symbol or press Enter");
            str = scannerString.nextLine();
        }
        Administrator.readAllProducts(products);
    }


    private static void createCategoriesFromConsole(List<Category> categories, Scanner scannerString) {
        System.out.println("[Administrator] Create new categories");
        String str = "";
        while(!str.equalsIgnoreCase("y")) {
            System.out.println("Category name");
            String categoryName = scannerString.nextLine();
            Administrator.createCategory(categoryName, categories);
            System.out.println("FINISH? If yes type \"y\". If not type another symbol or press Enter");
            str = scannerString.nextLine();
        }
        Administrator.readAllCategories(categories);
    }

}
