package ShopBackEnd;
import java.util.List;
import java.util.Queue;

public class Administrator {

    static public List<Product> addProduct(List<Product> products, List<Category> categoryList, String name, int index, double price, int quantity) {
        products.add(new Product(name, categoryList.get(index), price, quantity));
        return products;
    }

    static public void readAllProducts(List<Product> products) {
        int index = 0;
        for(Product product : products) {
            System.out.println("Index " + index++ + " " +product);
        }
    }

    static public void upgradeProduct(List<Product> products, int productIndex, List<Category> categoryList, int categoryIndex, String name, double price, int quantity) {
        products.get(productIndex).setName(name);
        products.get(productIndex).setCategory(categoryList.get(categoryIndex));
        products.get(productIndex).setPrice(price);
        products.get(productIndex).setQuantity(quantity);
    }

    static public List<Product> deleteProduct(List<Product> products, int productIndex) {
        products.remove(productIndex);
        return products;
    }


    static public List<Category> createCategory(String name, List<Category> categoryList) {
        categoryList.add(new Category(name));
        return categoryList;
    }


    static public void readAllCategories(List<Category> categoryList) {
        int index = 0;
        for(Category category : categoryList) {
            System.out.println("Index " + index++ + " " + category);
        }
    }

    static public void upgradeCategory(List<Category> categoryList,int index,  String name) {
        categoryList.get(index).setName(name);
    }

    static public List<Category> deleteCategory(List<Category> categoryList, int index) {
        categoryList.remove(index);
        return categoryList;
    }

    static public void viewPlacedOrders(Queue<List<Product>> orderList) {
        System.out.println("Placed orders : " + orderList);
    }

    static public List<List<Product>> approveOrders(List<List<Product>> approvedOrders, Queue<List<Product>> orderList) {
        if (orderList.size() > 0) {
            for (Product product : orderList.peek()) {
                product.setQuantity(product.getQuantity() - 1);
            }
            approvedOrders.add(orderList.poll());
        }
        return approvedOrders;
    }

    static public void viewApprovedOrders(List<List<Product>> approvedOrders) {
        System.out.println("Approved orders");
        for(List<Product> approvedListOfProduct : approvedOrders) {
            System.out.println(approvedListOfProduct);
        }
    }

    static public void viewProcessingOrders(Queue<List<Product>> orderList) {
        System.out.println("Processing orders");
        for(List<Product> processingListOfProducts : orderList) {
            System.out.println(processingListOfProducts);
        }
    }

    static public void viewTotalCost(List<List<Product>> approvedOrders, Queue<List<Product>> orderList) {
        double sum = 0;
        for(List<Product> approvedListOfProduct : approvedOrders) {
            for(Product product : approvedListOfProduct) {
                sum += product.getPrice();
            }
        }
        for(List<Product> orderedListOfProduct : orderList) {
            for(Product product : orderedListOfProduct) {
                sum += product.getPrice();
            }
        }
        System.out.println("Total cost of approved and placed orders : " + sum);
    }
}
