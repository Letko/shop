package ShopBackEnd;

public class Category {
    private String name;

    public Category(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Category{" +
                "name='" + name + '\'' +
                '}';
    }

    void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
