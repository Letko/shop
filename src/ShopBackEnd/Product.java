package ShopBackEnd;

import ShopBackEnd.Category;

public class Product {
    private String name;
    private Category category;
    private double price;
    private int quantity;

    Product(String name, Category category, double price, int quantity) {
        this.name = name;
        this.category = category;
        this.price = price;
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "ShopBackEnd.Product{" +
                "name='" + name + '\'' +
                ", category=" + category +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }

    void setName(String name) {
        this.name = name;
    }

    void setCategory(Category category) {
        this.category = category;
    }

    void setPrice(double price) {
        this.price = price;
    }

    void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    String getName() {
        return name;
    }

    Category getCategory() {
        return category;
    }

    double getPrice() {
        return price;
    }

    int getQuantity() {
        return quantity;
    }
}
