package ShopBackEnd;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Queue;

public class User  {

    private List<Product> basket;

    public User(List<Product> basket) {
        this.basket = basket;
    }

    public List<Product> getBasket() {
        return basket;
    }

    public void searchProductByName(List<Product> products, String name) {
        for(Product product : products) {
            if (name.equalsIgnoreCase(product.getName())) {
                System.out.println(product);
            }
        }
    }

    public void searchProductByCategory(List<Product> products, String name) {
        for(Product product : products) {
            if(name.equalsIgnoreCase(product.getCategory().getName())) {
                System.out.println(product);
            }
        }
    }

    public List<Product> sortProductByName(List<Product> products) {
        Collections.sort(products, Comparator.comparing(Product::getName));
        return products;
    }

    public void readSortedProducts(List<Product> products) {
        int index = 0;
        for(Product product : products) {
            System.out.println("Index " + index++ + " " + product);
        }
    }

    public List<Product> sortProductByPrice(List<Product> products) {
        Collections.sort(products, Comparator.comparing(Product::getPrice));
        return products;
    }

    public List<Product> addProductToBasket(List<Product> basket, List<Product> products, int index) {
        basket.add(products.get(index));
        return basket;
    }


    public Queue<List<Product>> addOrderToOrderList(Queue<List<Product>> orderList, List<Product> basket) {
        orderList.add(basket);
        return orderList;
    }








}
